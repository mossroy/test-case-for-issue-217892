package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MyTableRepository {
	
	@Autowired  
    private JdbcTemplate jdbc;
	
	public void addRecord() {
		jdbc.execute("insert into mytable (myfield) values('test')");
	}
		
	public int countRecords() {
		return jdbc.queryForObject("select count(*) from mytable", int.class);
	}

	public void flush() {
		jdbc.execute("delete from mytable");
	}
	
}	
